	<footer>
		<nav>
			<div>
				<a href="/products"><strong>Products</strong></a>
				<a href="/products">Overview</a>
				<a href="/models">Models</a>
				<a href="/accessories-and-add-ons">Accessories &amp; Add-Ons</a>
				<a href="/pricing-and-purchasing">Pricing Purchasing Options</a>
				<a href="/profiting">Profiting With Shockwave</a>
			</div><div>
				<a href="/shockwave-therapy"><strong>Science</strong></a>
				<a href="/shockwave-therapy">Shockwave Therapy</a>
				<a href="/treatable-conditions">Treatable Conditions</a>
				<a href="/scientific-evidence-and-case-studies">Scientific Evidence &amp; Case Studies</a>
				<a href="/testimonials">Testimonials</a>
			</div><div>
				<a href="/support"><strong>Services</strong></a>
				<a href="/support">Support</a>
				<a href="/training-and-education">Training &amp; Education</a>
			</div><div>
				<a href="/marketing-materials"><strong>Resources</strong></a>
				<a href="/marketing-materials">Marketing Materials</a>
				<a href="/press">Press</a>
			</div><div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				<a href="/contact-us"><strong>Contact us</strong></a>
				<a href="https://goo.gl/maps/dCsjf">
					<span itemprop="name">Shockwave Canada</span><br>
					<span itemprop="streetAddress">89 Humber College Blvd., Suite 106</span><br>
					<span itemprop="addressLocality">Toronto</span>,
					<span itemprop="addressRegion">ON</span>
					<span itemprop="postalCode">M9V 1B8,</span>
					<span itemprop="addressCountry">Canada</span>
				</a>
				<a itemprop="telephone" href="tel:+14167417462">Phone: (416) 741-SHOC(7462)</a>
				<a href="tel:+18887417462">Toll Free: (888) 741-SHOC(7462)</a>
				<a href="tel:+14167418424">Fax: (416) 741-8424</a>
				<a itemprop="email" href="mailto:brian@shockwavecanadainc.ca">brian@shockwavecanadainc.ca</a>
			</div>
		</nav>

		<a href="/" class="footer-copyright">&copy;<?=date('Y');?> Shockwave Canada Inc.</a>
	</footer>

	<script async src="//cdn.polyfill.io/v1/polyfill.js"></script>
	<!--[if lt IE 9]>
		<script async src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script async src="//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
	<![endif]-->
</body>
</html>
<?php ob_end_flush(); // Send the Output Buffering ?>
