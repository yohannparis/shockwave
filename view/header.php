<?php
	error_reporting(0); // Error Reporting
	ob_start('ob_gzhandler'); // Compress output and turn on buffer
	header('content-type:text/html; charset=utf-8'); // Set type and charset

	if(!isset($page) or $page != 'home'){$title = ucwords($page).' - '; } else { $title = ''; }
	if(!isset($description)){ $description = 'The leader in shockwave therapy'; }
	if(!isset($menu)){ $menu = ''; }

?><!DOCTYPE html>
<html lang="en-US" itemscope itemtype="http://schema.org/LocalBusiness">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php /*<link rel="stylesheet" media="all" type="text/css" href="/view/css/<?=$page;?>.css"> */ ?>
	<style type="text/css"><?php include "view/css/$page.css"; ?></style>

	<title><?=$title;?>Shockwave Canada Inc.</title>
	<meta itemprop="url" property="og:url" content="http://shockwavecanadainc.com/">
	<meta itemprop="image" property="og:image" content="http://shockwavecanadainc.com/view/images/logo.png">
	<meta itemprop="name" property="og:site_name" content="Shockwave Canada Inc.">
	<meta itemprop="description" name="description" property="og:title" content="<?=$description;?>">
	<meta property="og:description" content="<?=$description;?>">
	<meta property="og:locale" content="en_US">
</head>
<body class="<?=$page;?>">
	<nav class="main-navigation">
		<ul>
			<li><img src="/view/images/logo.png" alt="Shockwave Canada Inc."></li>
			<li><a href="/">Home</a></li>
			<li class="sub-menu <?php if($menu === 'products'){ echo 'active'; }?>">
				<a href="/products">Products</a>
				<nav>
					<a href="/products">Overview & key features</a>
					<a href="/models">Models</a>
					<a href="/mp50" class="sub-menu-item">MP50</a>
					<a href="/mp100" class="sub-menu-item">MP100</a>
					<a href="/mp200" class="sub-menu-item">MP200</a>
					<a href="/compare-models" class="sub-menu-item">Compare &amp; see all models</a>
					<a href="/models-vs-competitors" class="sub-menu-item">Shockwave vs. competitors</a>
					<a href="/accessories-and-add-ons">Accessories &amp; add-ons</a>
					<a href="/pricing-and-purchasing">Pricing &amp; purchasing</a>
					<a href="/profiting">Profiting with shockwave</a>
				</nav>
			</li>
			<li class="sub-menu <?php if($menu === 'services'){ echo 'active'; }?>">
				<a href="/support">Services</a>
				<nav>
					<a href="/support">Support</a>
					<a href="/training-and-education">Training &amp; Education</a>
				</nav>
			</li>
			<li class="sub-menu <?php if($menu === 'resources'){ echo 'active'; }?>">
				<a href="/marketing-materials">Resources</a>
				<nav>
					<a href="/marketing-materials">Marketing Materials</a>
					<a href="/press">Press</a>
				</nav>
			</li>
			<li class="sub-menu <?php if($menu === 'science'){ echo 'active'; }?>">
				<a href="/shockwave-therapy">Science</a>
				<nav>
					<a href="/shockwave-therapy">About shockwave therapy</a>
					<a href="/treatable-conditions">Treatable conditions &amp; effectiveness</a>
					<a href="/scientific-evidence-and-case-studies">Scientific evidence &amp; case studies</a>
					<a href="/testimonials">Testimonials</a>
				</nav>
			</li>
			<li><a href="/contact-us">Contact Us</a></li>
		</ul>
	</nav>
