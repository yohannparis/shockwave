<div class="models-footer">
	<div>
		<h3>Models</h3>
		<a href="/mp50">
			<img src="/model/images/products_models-mp50.jpg" alt="MP50">
			MASTERPULS «ULTRA» MP50
		</a>
		<a href="/mp100">
			<img src="/model/images/products_models-mp100.jpg" alt="MP100">
			MASTERPULS «ULTRA» MP100
		</a>
		<a href="/mp200">
			<img src="/model/images/products_models-mp200.jpg" alt="MP200">
			MASTERPULS «ULTRA» MP200
		</a>
	</div>
</div>
