<?php
	$page = 'marketing-materials';
	$menu = 'resources';
	require_once 'view/header.php';

	$show_hidden = false;
	if (isset($_POST['password']) && $_POST['password'] === 'Shockwave_9550'){
		$show_hidden = true;
	}
?>

<article class="article-center">
	<h1>You’ve got the means, now show them the how</h1>
	<p>
		Informing patients of a non-invasive road to recovery is a lot easier when patients
		understand their options. With our customizable pamphlets, your clinic’s name and
		logo can give the credibility patient’s need in considering shockwave therapy.
	</p>
	<p><strong>
		Discover the tools our medical practitioners use to add value to their patient care.
	</strong></p>
</article>

<article class="article-center">
	<h2>To download the pamphlets or the images in high quality you need to enter a password.</h2>
	<form action="marketing-materials" method="post">
		<input type="text" name="password"/><input type="submit" name="submit" value="SUBMIT" />
	</form>
</article>

<article class="article-center">
	<h2>Pamphlets</h2>
	<p>
		The pamphlets can be customized to your needs with your logo, and clinic information.
		Just save the file on a CD, pen drive, or memory card, and take it to a printer.
	</p>

	<div>
		<h3>Ultra</h3>
		<ul>
			<?php
				$pam_links = array ('Shockwave_PatientWideWhite_FR', 'Shockwave_PatientWideWhite_EN', 'Shockwave_PatientNarrowWhite_FR', 'Shockwave_PatientNarrowWhite_EN', 'Shockwave_PatientLetter_FR', 'Shockwave_PatientLetter_EN');
				$pam_name = array ( 'Wide White FR', 'Wide White EN', 'Narrow White FR', 'Narrow White EN', 'Letter FR', 'Letter FR');
				for ($i=0; $i < 6 ; $i++) {
			?>
				<li>
					<img src="/model/marketing-materials/pamphlets/ultra/<?=$pam_links[$i];?>.jpg" alt="" width="230" height="153"/>
					<span><?=$pam_name[$i];?> - Ultra</span>
					<?php if($show_hidden){ ?><a target="_blank" href="/model/marketing-materials/pamphlets/ultra/<?=$pam_links[$i];?>.pdf">Download</a><?php } ?>
				</li>
			<?php } ?>
		</ul>
	</div>

	<div>
		<h3>Elite</h3>
		<ul>
			<?php
				$pam_hidden_links = array ('01.zip', '01-fr.zip', '02.zip', '03.zip', '03-fr.zip', '04.zip', '05.zip', '06.zip');
				$pam_dimensions   = array ('8.5" x 3.6"', '8.5" x 3.6" - FR', '8.5" x 3.6"', '8.5" x 11"', '8.5" x 11" - FR', '3.6" x 8.5"', '3.6" x 8.5"', '3.6" x 8.5"');
				for ($i=0; $i < count($pam_hidden_links); $i++) {
			?>
				<li>
					<img src="/model/marketing-materials/pamphlets/elite/p-<?=$i;?>.png" alt="" width="230" height="153"/>
					<span><?=$pam_dimensions[$i];?> - Elite</span>
					<?php if($show_hidden){ ?><a target="_blank" href="/model/marketing-materials/pamphlets/elite/<?=$pam_hidden_links[$i];?>">Download</a><?php } ?>
				</li>
			<?php } ?>
		</ul>
	</div>
</article>

<article class="article-center">
	<h2>Images</h2>
	<p>
		If you would like to create your own pamphlets you can download the image files
		and start the artwork from scratch.
	</p>

	<div>
		<h3>Ultra</h3>
		<ul>
			<?php
				for ($i=1; $i < 10 ; $i++) {
			?>
				<li>
					<img src="/model/marketing-materials/images/ultra/MASTERPULS_ultra_<?=$i;?>-thumb.jpg" alt="" width="230" height="153"/>
					<span>Image <?=$i;?> - Ultra</span>
					<?php if($show_hidden){ ?><a target="_blank" href="/model/marketing-materials/images/ultra/MASTERPULS_ultra_<?=$i;?>.jpg">Download</a><?php } ?>
				</li>
			<?php } ?>
		</ul>
	</div>

	<div>
		<h3>Elite</h3>
		<ul>
			<?php
				$img_public_links = array ('small-img-1.jpg', 'small-img-2.jpg', 'small-img-3.jpg', 'small-img-4.jpg', 'small-img-5.jpg', 'small-img-6.jpg');
				$img_hidden_links = array ('hip-area-radial.jpg', 'shockwave-doctor.jpg', 'knee-radial.jpg', 'shockwave-mp50.jpg', 'neck-radial.jpg', 'lower-leg-radial.jpg');
				for ($i=0; $i < count($img_hidden_links); $i++) {
			?>
				<li>
					<img src="/model/marketing-materials/images/elite/<?=$img_public_links[$i];?>" alt="" width="230" height="153"/>
					<span>Image <?=$i;?> - Elite</span>
					<?php if($show_hidden){ ?><a target="_blank" href="/model/marketing-materials/images/elite/<?=$img_hidden_links[$i];?>">Download</a><?php } ?>
				</li>
			<?php } ?>
		</ul>
	</div>

</article>

<article class="article-center">
	<h2>Videos</h2>
	<ul>
		<li>
			<img src="/model/marketing-materials/videos/Shockwave_1400kbps.png" alt="" width="230" height="153"/>
			<span>Patient Education</span>
			<?php if($show_hidden){ ?><a target="_blank" href="/model/marketing-materials/videos/Shockwave_1400kbps.mp4">Download</a><?php } ?>
		</li>
	</ul>
</article>

<?php include 'view/modules/models-footer.php'; ?>
<?php require_once 'view/footer.php'; ?>
