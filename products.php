<?php
	$page = 'products';
	$menu = 'products';
	require_once 'view/header.php';
?>

<section>
	<h1>The exclusive distributor of storz radial devices in canada</h1>
	<p>
		Shockwave Canada believes in better therapies through new technology.
		Storz has been a leading manufacturer of medical equipment since 1941. Their
		mission is to continually improve shockwave technology, to develop new system
		concepts, and to identify new therapy indications in close cooperation with
		leading medical institutes.
	</p>
	<p><strong>
		Discover how Shockwave Canada leverages over 75 years of medical research
		and development through world-renowned medical manufacturer, STORZ.
	</strong></p>
</section>

<article>
	<img src="/model/images/products_overview-newdesign.jpg" alt="products_overview-newdesign">
	<span>
		<h2>New generation portable shockwave device</h2>
		<p>
			The pioneering »Single Frame Casing« of the MASTERPULS® »ultra« is impressive because of
			its innovative three-dimensional design. Clear lines and a front that resembles a shock
			wave signal power, reliability and path-breaking&nbsp;efficiency.
		</p>
	</span>
</article>

<article>
	<span>
		<h2>New generation r-sw hand-controlled handpiece</h2>
		<p>
			Newly developed hand control, with all main operating elements integrated into the handpiece.
			This allows for safer treatments on patients as changes can be made to settings without looking
			away from the patient. Frequency, energy levels and number of shocks applied can be adjusted
			directly via selector buttons. Only product of its kind on the&nbsp;market
		</p>
	</span>
	<img src="/model/images/products_overview-newhandpiece.jpg" alt="products_overview-newhandpiece">
</article>

<article>
	<img src="/model/images/products_overview-vactor.jpg" alt="products_overview-vactor">
	<span>
		<h2>V-Actor handpiece</h2>
		<p>
			Enables targeted application of pneumatically generated vibration pulses to the tissue.
			Microcirculation is improved by compression and decompression, and metabolic waste products
			and toxins are removed via the blood and lymph system. Elongation of fascial and muscle fibres
			restores normal muscle tone as muscle shortening and hardening is&nbsp;eliminated.
		</p>
	</span>
</article>

<article>
	<h2>About STORZ</h2>
	<p><strong>
		Established in 1941, the objective pursued by STORZ physicists and engineers is to continuously
		improve medical equipment including shock wave technology, to develop new system concepts and
		to identify new therapy indications in close co-operation with leading medical&nbsp;institutes.
	</strong></p>
	<p>
		STORZ products have proved their efficacy in urology in millions of cases. For many years, their
		wide spectrum of possibilities and the unique benefits offered by non-invasive shock wave technology
		has also been extended to other medical disciplines. With its state-of-the-art technological innovations,
		STORZ has set the standards in orthopaedics, rehabilitation medicine, aesthetic medicine and
		veterinary&nbsp;medicine.
	</p><p>
		Pioneering achievements such as the invention of the electromagnetic cylindrical source, the
		first application of shock waves in cardiac revascularization and combined shock wave therapy
		(focused, planar and radial shock waves) provide ample proof of the vast performance spectrum
		of STORZ in the field of shock wave&nbsp;therapy.
	</p>
</article>

<?php include 'view/modules/models-footer.php'; ?>
<?php include 'view/footer.php'; ?>
