<?php
	$page = 'press';
	$menu = 'resources';
	require_once 'view/header.php';
?>

<article class="article-center">
	<h1>Shockwave results are making headlines</h1>
</article>

<article>
	<ul class="page-press">
		<?php
			// The press
			$press[] = array('title' => 'MASSAGE THERAPY CANADA - FALL 2012', 'msg' => 'Radial Shockwave therapy - An integrated approach',
											 'src1' => 'massagefall-2012-shockwave-1-1.jpg', 'src2' => 'massagefall-2012-shockwave-1-2.jpg',
											 'src3' => 'nothing.png', 'src4' => 'nothing.png',
											 'pdf' => 'massagefall-2012-shockwave.pdf');

			$press[] = array('title' => 'London Olympics 2012', 'msg' => 'Shockwave Therapy being used by team USA.',
											 'src1' => 'london.png', 'src2' => 'nothing.png',
											 'src3' => 'nothing.png', 'src4' => 'nothing.png',
											 'pdf' => 'london.png');

			$press[] = array('title' => 'TORONTO SUN', 'msg' => 'According to an American Podiatric Medical Association study, 39% of women wear high heels every day, and 75% report shoe-related foot problems. Two-thirds admit to wearing ill-fitting shoes.',
											 'src1' => 'TORONTO-SUN-1.png', 'src2' => 'TORONTO-SUN-2.png',
											 'src3' => 'nothing.png', 'src4' => 'nothing.png',
											 'pdf' => 'TORONTO-SUN.pdf');

			$press[] = array('title' => 'TORONTO SUN', 'msg' => 'Shock wave therapy on Peter Worthington’s painful heel hasn’t improved his tennis ame but has at least allowed him to participate',
											 'src1' => 'worthington-new.jpg', 'src2' => 'nothing.png',
											 'src3' => 'nothing.png', 'src4' => 'nothing.png',
											 'pdf' => 'worthington-new.jpg');

			$press[] = array('title' => 'CANADIAN CHIROPRACTOR MAGAZINE', 'msg' => 'The Results of Radial Shockwave Therapy',
											 'src1' => 'canadian-chiropractor1.png', 'src2' => 'canadian-chiropractor2.png',
											 'src3' => 'canadian-chiropractor3.png', 'src4' => 'canadian-chiropractor4.png',
											 'pdf' => 'canadian-chiropractor.pdf');

			$press[] = array('title' => 'FEATURE', 'msg' => 'Update on Shockwave Therapy',
											 'src1' => 'press-shockwave-article-1.jpg', 'src2' => 'press-shockwave-article-2.jpg',
											 'src3' => 'nothing.png', 'src4' => 'nothing.png',
											 'pdf' => 'press-shockwave-article.pdf');

			foreach ($press as $key => $article) {
		?>
			<li data-src1="<?=$article['src1'];?>" data-src2="<?=$article['src2'];?>"
					data-src3="<?=$article['src3'];?>" data-src4="<?=$article['src4'];?>"
					data-download="<?=$article['pdf'];?>">
				<span><?=$article['title'];?></span>
				<?=$article['msg'];?>
			</li>
		<?php } ?>
	</ul>

	<div>
		<img id="page-press-image1" src="/model/press/<?=$press[0]['src1'];?>" alt="<?=$press[0]['title'];?>">
		<img id="page-press-image2" src="/model/press/<?=$press[0]['src2'];?>" alt="<?=$press[0]['title'];?>">
		<img id="page-press-image3" src="/model/press/<?=$press[0]['src3'];?>" alt="<?=$press[0]['title'];?>">
		<img id="page-press-image4" src="/model/press/<?=$press[0]['src4'];?>" alt="<?=$press[0]['title'];?>">
		<a href="javascript:void(0)" class="page-press-arrow prev">&lt; Previous</a>
		<a id="page-press-download" href="/model/press/<?=$press[0]['pdf'];?>">Download</a>
		<a href="javascript:void(0)" class="page-press-arrow next">Next &gt;</a>
	</div>
</article>

<script type="text/javascript">
  function move(direction) {
    var list = document.querySelectorAll('.page-press li'); // Get the list of artworks
    var current = document.querySelector('.page-press .active'); // Get the current one
    current.classList.remove('active'); // Remove is marker

    switch (direction){ // Select what direction we are going

      case 'next': // If we are moving forward
        var index = Array.prototype.slice.call(list).indexOf(current); // Position of the current one
        if (index >= list.length-1){ // But we are at the end of the list
          var next = list[0]; // go back to the beginning
        } else {
          var next = list[index+1]; // keep moving forward
        }
        break;

      case 'prev': // If we are going backward
        var index = Array.prototype.slice.call(list).indexOf(current); // Position of the current one
        if (index < 1){ // if we are at the beggining
          var next = list[list.length-1]; // go to the end
        } else {
          var next = list[index-1]; // keep moving backward
        }
        break;

      default: // Or it's a specific direction
        var index = Array.prototype.slice.call(list).indexOf(direction); // Find the position of the specific one
        var next = list[index]; // display the selected one
        break;
    }

    next.classList.add('active'); // Add the marker
    document.querySelector('#page-press-image1').src = '/model/press/' + next.getAttribute('data-src1'); // Show the image
    document.querySelector('#page-press-image2').src = '/model/press/' + next.getAttribute('data-src2'); // Show the image
    document.querySelector('#page-press-image3').src = '/model/press/' + next.getAttribute('data-src3'); // Show the image
    document.querySelector('#page-press-image4').src = '/model/press/' + next.getAttribute('data-src4'); // Show the image
		document.querySelector('#page-press-download').src = '/model/press/' + next.getAttribute('data-download'); // Show the image
  }

  window.onload = function() {
    var images = document.querySelectorAll('.page-press li');
    images[0].classList.add('active'); // Display the first image

    // Go directly to the appropriate image
    for (var i = images.length - 1; i >= 0; i--) {
      images[i].addEventListener('click', function(){
        move(this);
      });
    };

    // On the click of one of the arrow
    document.querySelector('.page-press-arrow.prev').addEventListener('click', function() { move('prev'); });
    document.querySelector('.page-press-arrow.next').addEventListener('click', function() { move('next'); });

    // On the press of one of the keyboard arrow
    document.onkeydown = function(e) {
      e = e || window.event;
      switch (e.keyCode) {
        case 37: // Left key
          move('prev');
          break;
        case 39: // Right key
          move('next');
          break;
      }
    };
  };
</script>

<?php include 'view/modules/models-footer.php'; ?>
<?php require_once 'view/footer.php'; ?>
