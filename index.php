<?php
	$page = 'home';
	require_once 'view/header.php';
?>

<article class="article-center">

	<div>
		<h1>The leader in shockwave therapy</h1>
		<p>
			No one sells more shockwave equipment to more healthcare practitioners, than Shockwave Canada.
			From providing the very best equipment and superior service, to offering the most accessible units
			at an affordable price, we continually strive to offer our customers the absolute best to better
			care for their&nbsp;patients.
		</p>
		<p><strong>
			We invite you to discover why Shockwave Canada is chosen over any other shockwave&nbsp;equipment.
		</strong></p>
	</div>

	<img src="/model/images/home_main.jpg" alt="MP200">

</article>

<ul class="home-testimonials">
	<a href="javascript:void(0)" class="home-testimonials-arrow prev">&lt;</a>
	<?php
		$testimonials[] = array('link' => '01', 'quote' => 'Most soft tissue injuries resolve within 4-5 sessions and most people notice an improvement even after the first session.');
		$testimonials[] = array('link' => '02', 'quote' => 'I am a practitioner that is skeptical of everything…after 2 + million shocks and counting, the results are nothing short of astounding.');
		$testimonials[] = array('link' => '03', 'quote' => 'A patient was referred to our office who was scheduled for surgical intervention of a frozen shoulder…after 3 sessions she had full range of motion. The surgeons office didn’t believe that she no longer required surgery…');
		$testimonials[] = array('link' => '08', 'quote' => 'Not only is the treatment effective clinically but it enhances your practice economically. The treatment is covered fully or partially by insurance companies and the Workers Compensation Board.');
		$testimonials[] = array('link' => '10', 'quote' => 'Ever since I started integrating shockwave therapy into my treatment plans my patients have been getting better faster.');
		$testimonials[] = array('link' => '11', 'quote' => 'My worst case involved a fellow who had a steel plate fall onto his back 40 years ago. After the first treatment he was pain free for the first time in 40 years.');
		$testimonials[] = array('link' => '12', 'quote' => 'We have treated hundreds of patients with Shockwave therapy achieving greater than 90% success.');

		foreach ($testimonials as $testimonial) {
	?>
		<li>
			<p>&ldquo;<?=$testimonial['quote'];?>&rdquo;</p>
			<a href="/testimonials?t=<?=$testimonial['link'];?>">Click to read the full testimonial</a>
		</li>
	<?php } ?>
  <a href="javascript:void(0)" class="home-testimonials-arrow next">&gt;</a>
</ul>

<section>
	<span>
		<a href="/models-vs-competitors">How we surpass the competition</a>
		<img src="/model/images/home_surpass.jpg" alt="How we surpass the competition">
	</span>
	<span>
		<a href="/profiting">Profiting with shockwave</a>
		<img src="/model/images/home_protifing.jpg" alt="Profiting with shockwave">
	</span>
	<span>
		<a href="/scientific-evidence-and-case-studies">The proof is in the science</a>
		<img src="/model/images/home_science.jpg" alt="The proof is in the science">
	</span>
</section>

<script type="text/javascript">
  function move(direction) {
    var list = document.querySelectorAll('.home-testimonials li'); // Get the list of artworks
    var current = document.querySelector('.active'); // Get the current one
    current.classList.remove('active'); // Remove is marker

    switch (direction){ // Select what direction we are going

      case 'next': // If we are moving forward
        var index = Array.prototype.slice.call(list).indexOf(current); // Position of the current one
        if (index >= list.length-1){ // But we are at the end of the list
          var next = list[0]; // go back to the beginning
        } else {
          var next = list[index+1]; // keep moving forward
        }
        break;

      case 'prev': // If we are going backward
        var index = Array.prototype.slice.call(list).indexOf(current); // Position of the current one
        if (index < 1){ // if we are at the beggining
          var next = list[list.length-1]; // go to the end
        } else {
          var next = list[index-1]; // keep moving backward
        }
        break;

      default: // Or it's a specific direction
        var index = Array.prototype.slice.call(list).indexOf(direction); // Find the position of the specific one
        var next = list[index]; // display the selected one
        break;
    }

    next.classList.add('active'); // Add the marker
  }

  window.onload = function() {
    var images = document.querySelectorAll('.home-testimonials li');
    images[0].classList.add('active'); // Display the first image

    // Go directly to the appropriate image
    for (var i = images.length - 1; i >= 0; i--) {
      images[i].addEventListener('click', function(){
        move(this);
      });
    };

    // Go trough the images every 10 sec
    setInterval(function(){move('next');}, 5000);

    // On the click of one of the arrow
    document.querySelector('.home-testimonials-arrow.prev').addEventListener('click', function() { move('prev'); });
    document.querySelector('.home-testimonials-arrow.next').addEventListener('click', function() { move('next'); });

    // On the press of one of the keyboard arrow
    document.onkeydown = function(e) {
      e = e || window.event;
      switch (e.keyCode) {
        case 37: // Left key
          move('prev');
          break;
        case 39: // Right key
          move('next');
          break;
      }
    };
  };
</script>

<?php require_once 'view/footer.php'; ?>
