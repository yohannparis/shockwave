<?php
	$page = 'mp';
	$menu = 'products';
	require_once 'view/header.php';
?>

<article>
	<img src="/model/images/products_mp50-main.jpg" alt="MP50">
	<h3>Models</h3>
	<h1>MASTERPULS «ULTRA» MP50</h1>
	<p>
		Compact design, low capital expenditure, maximum portability. The MP50 features a
		newly designed hand control with all the main operating elements integrated into
		the hand-piece, allowing for safer treatment on patients as changes can made to
		settings without looking away. Multiple handpicks are optional. Featuring deep
		impact transmitters.
	</p>

	<span>
		<a href="/model/products/mp50brochure.pdf">Download English Brochure</a>
		<a href="/model/products/mp50brochurefr.pdf">Download French Brochure</a>
		<a href="/model/products/mp50flyer.pdf">Download English Flyer</a>
		<a href="/model/products/mp50booklet.pdf">Download English Booklet</a>
	</span>
</article>

<article>

	<div>
		<h3>Key Features</h3>
		<ul>
			<li>Compact design</li>
			<li>Low capital expenditure and maintenance costs</li>
			<li>Maximum mobility, can be easily carried anywhere</li>
			<li>
				Newly developed hand control, with all main operating elements integrated into the handpiece.
				Frequency, energy levels and number of shocks applied can be adjusted directly via selector buttons.
				Only product of its kind on the market. This allows for safer treatments on patients as changes can be
				made to settings without looking away from the patient
			</li>
			<li>Vibration therapy (V-ACTOR.)</li>
		</ul>
		<a href="/accessories-and-add-ons">View optional accessories</a>
	</div>

	<div>
		<h3>Specifications</h3>
		<p>
			<strong>Facts &amp; Figures</strong>
			<br>• Compact and mobile: 9.5 kg
			<br>• Built-in compressor
			<br>• Control and display all on handpiece • Hand operated Shockwave devices
		</p><p>
			<strong>Parameters</strong>
			<br>• Shock frequency: 1 – 15 Hz
			<br>• Application pressure: 1 – 4 bar/11 MPa
		</p><p>
			<strong>Oscillating 'D-ACTOR&reg;' Technology</strong>
			<br>• For better myofascial Trigger Point therapy
		</p><p>
			<strong>'Deep Impact' Shock Transmitter</strong>
			<br>• For the treatment of deep pain regions
		</p><p>
			<strong>'CERAma-xTM' Shock Transmitter</strong>
			<br>• Elastic shock transmitter for Shockwave
		</p>

	</div>

</article>

<?php include 'view/modules/compare-footer.php'; ?>
<?php require_once 'view/footer.php'; ?>
