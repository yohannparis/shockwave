<?php
	$page = 'shockwave-therapy';
	$menu = 'science';
	require_once 'view/header.php';
?>

<article class="article-center">
	<h1>How shockwave therapy really works.</h1>
	<p>
		Shockwave therapy accelerates the healing process in the body by stimulating
		the metabolism and enhancing blood circulation to regenerate damaged tissue.
		Strong energy pulses are applied to the affected area. These pulses occur for
		short periods of time, creating micro-cavitation bubbles that expand and burst.
		The force created by these bubbles penetrates tissue and stimulates cells in the
		body that are responsible for bone and connective tissue healing. In many instances,
		shockwave therapy is most effective in cases where the human body has not been able
		to heal itself on its own.
	</p>
</article>

<article>
	<div>
		<img src="/model/images/science_howitworks-technology.jpg" alt="science_howitworks-technology">
		<h2>Technology</h2>
		<p>
			Extracorporeal shock wave therapy (ESWT) is a modern and highly effective treatment
			method: high-energy sound waves are introduced into the painful areas of the body.
			With this innovative therapy approach, pathological alterations of tendons, ligaments,
			capsules, muscles and bones can be eliminated systematically.
		</p><p>
			STORZ offers focused, planar and radial shock wave systems to cover the great variety
			of indications in orthopaedic therapy and rehabilitation medicine and makes the combined
			shock wave therapy system DUOLITH® SD1 and the radial system MASTERPULS® line the most
			successful ESWT systems worldwide today.
		</p>
	</div>
	<div>
		<img src="/model/images/science_howitworks-benefits.jpg" alt="science_howitworks-benefits">
		<h2>Benefits</h2>
		<p>
			There are numerous benefits from shockwave therapy.
			<br>• Non-Surgical
			<br>• No Side Effects
			<br>• Accelerated Healing
			<br>• Ease Of Use
			<br>• Affordable
			<br>• Covered By Most Insurance Companies
		</p><p>
			The beneficial effects of shock wave therapy are often experienced after only 1 or 2
			treatments. The therapy eliminates pain and restores full mobility, thus improving
			your quality of life.
		</p>
	</div>
</article>

<article>
	<h2>Proposed mechanism of EWST</h2>
	<img src="/model/images/science_howitworks-ewstdiagram.png" alt="science_howitworks-ewstdiagram">
	<p>
		Source: Dr. Wang<br>
		Extracorporeal shockwave therapy clinical results, technilogy basics, GERDESMEYER 2007, page 52
	</p>
</article>

<?php include 'view/modules/models-footer.php'; ?>
<?php require_once 'view/footer.php'; ?>
