<?php
	$page = 'mp';
	$menu = 'products';
	require_once 'view/header.php';
?>

<article>
	<img src="/model/images/products_mp200-main.jpg" alt="MP200">
	<h3>Models</h3>
	<h1>MASTERPULS «ULTRA» MP200</h1>
	<p>
		The ultimate in comprehensive care, the MP 200 provides more ways than any MasterPuls
		unit to care for your patients. Featuring deep impact shock transmitters, it accurately
		pinpoints deep pain regions that can’t be reached by other shockwave device manufacturers.
	</p>

	<?php /* <span>
		<a href="/model/products/mp200brochure.pdf">Download English Brochure</a>
	</span> */ ?>
</article>

<article>

	<div>
		<h3>Key Features</h3>
		<ul>
			<li>Compact design</li>
			<li>Pioneering Single Frame Casing</li>
			<li>Maximum mobility, can be easily carried anywhere</li>
			<li>Built-in high-performance »Silent« compressor with time controlled condensate drain</li>
			<li>All-new R-SW hand-controlled handpiece with innovative »Active-tip-control« display with pressure and frequency selection</li>
			<li>Individual Parameter Setting (IPS-Control) for different indications</li>
			<li>Combinable handpieces</li>
			<li>Radial Shockwave therapy with various shock transmitters</li>
			<li>Oscillating ‘D-ACTOR®’ Technology for better myofascial Trigger Point therapy</li>
			<li>V-ACTOR® vibration therapy: 21 – 31 Hz (optional)</li>
		</ul>
		<a href="/accessories-and-add-ons">View optional accessories</a>
	</div>

	<div>
		<h3>Specifications</h3>
		<p>
			<strong>Facts &amp; Figures</strong>
			<br>• ‘Easy’ transport bag
			<br>• System weight: 9.5 kg
			<br>• Extended frequency/power range: 12 Hz/5.0 bar, 16 Hz/4.5 bar, 21 Hz/4.0 bar
		</p><p>
			<strong>Pulse Frequency/Pressure</strong>
			<br>• Radial shock wave therapy: 1 – 21 Hz/1 – 5 bar
			<br>• Vibration therapy (V-ACTOR®): 1 – 35 Hz/1 – 5 bar
		</p><p>
			<strong>Oscillating ‘D-ACTOR®’ Technology</strong>
			<br>• For better myofascial Trigger Point therapy
		</p><p>
			<strong>'Deep Impact' Shock Transmitter</strong>
			<br>• For the treatment of deep pain regions
		</p><p>
			<strong>'CERAma-xTM' Shock Transmitter</strong>
			<br>• Elastic shock transmitter for Shockwave
		</p>

	</div>

</article>

<?php include 'view/modules/compare-footer.php'; ?>
<?php require_once 'view/footer.php'; ?>
