<?php
	$page = 'support';
	$menu = 'services';
	require_once 'view/header.php';
?>

<article class="article-center">
	<h1>You don’t satisfy 100% of your customers, without giving 100%</h1>
	<p>
		Since its inception, we’ve strived to provide health care practitioners
		with nothing but the best in equipment and service. It’s why we’ve never
		had a single unit returned in the history of Shockwave Canada. Not one.
		We provide our customers with everything they need to implement shockwave
		treatment into their practice, to provide technologically advanced care
		complete with 24/7 support. And only Shockwave Canada gives exclusive
		access to a myriad of world-renowned healthcare advisors at no charge.
	</p>
	<p><strong>
		Discover how you can join the ranks of 100% satisfied customers.
	</strong></p>
</article>

<article>
	<div>
		<img src="/model/images/services_support-support.png" alt="services_support-support">
		<h2>24/7 SUPPORT</h2>
		<p>Around the clock support from world-renown healthcare practitioners.</p>
	</div>
	<div>
		<img src="/model/images/services_support-training.png" alt="services_support-training">
		<h2>TRAINING &amp; EDUCATION</h2>
		<p>Access to training videos &amp; tutorials, plus support whenever needed.</p>
	</div>
	<div>
		<img src="/model/images/services_support-resources.png" alt="services_support-resources">
		<h2>ACCESS TO RESOURCES</h2>
		<p>
			Customizable marketing materials to promote shockwave therapy at your practice,
			and unlimited access to our library of patient brochures.
		</p>
	</div>
</article>

<?php include 'view/modules/models-footer.php'; ?>
<?php require_once 'view/footer.php'; ?>
