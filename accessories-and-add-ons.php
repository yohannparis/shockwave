<?php
	$page = 'accessories-and-add-ons';
	$menu = 'products';
	require_once 'view/header.php';
?>

<article class="article-center">
	<img src="/model/images/products_accessories-main.jpg" alt="accesories">
	<h1>Accessories &amp; Add-ons</h1>
	<p>
		To provide the ultimate experience in shockwave therapy sometimes requires specialized equipment.
		Storz offers a wide array of accessories to provide patients with the best in comfort
		and care, while providing healthcare practitioners with unparalleled portability and
		convenience.
	</p>
</article>

<article>
	<img src="/model/images/products_accessories-vactor.jpg" alt="products_overview-vactor">
	<span>
		<h2>V-Actor handpiece</h2>
		<p>
			The main operating elements have been integrated into the »Active-tip-control« display
			of the handpiece and thus make working on the patient easier. The »Individual Parameter
			Setting« (IPS-Control) for all ESWT indications includes treatment parameters recommended
			by experienced users and ensures reliable pre-setting.
		</p>
	</span>
</article>

<article class="article-center">
	<div>
		<h2>4 fascia transmitter<br>heads &amp; case</h2>
		<img src="/model/images/products_accessories-fascia_transmitterheads.jpg" alt="products_accessories-fascia_transmitterheads">
	</div>
	<div>
		<h2>3 spine transmitter<br>heads &amp; case</h2>
		<img src="/model/images/products_accessories-spine_transmitterheads.jpg" alt="products_accessories-spine_transmitterheads">
	</div>
</article>

<article>
	<img src="/model/images/products_accessories-transmitterheads.jpg" alt="products_accessories-transmitterheads">
	<span>
		<h2>9 Shockwave transmitter heads &amp; case</h2>
		<p>
			STORZ has developed a transmitter programme especially for radial shock wave therapy and
			tailor-made for individual ESWT indications. State-of-the-art transmitter materials are
			used to optimize the transmission rate of shock wave energy into the pain region.
		</p><p>
			At the same time, energy losses at the skin coupling surface are minimized. This research
			work has led to optimal treatment successes with the MASTERPULS® »ultra«.
		</p>
		<a href="/model/pdf/Shockwave Transmitters PDF.pdf">Download Brochure</a>
	</span>
</article>

<article class="article-center">
	<div>
		<h2>Transport case</h2>
		<img src="/model/images/products_accessories-koffercase.jpg" alt="products_accessories-koffercase">
	</div>
	<div>
		<h2>TASCHE transport bag</h2>
		<img src="/model/images/products_accessories-taschebag.jpg" alt="products_accessories-taschebag">
	</div>
</article>

<article class="article-center">
	<h2><a href="/contact-us">
		Click here to Contact shockwave canada for pricing
	</a></h2>
</article>

<?php include 'view/modules/models-footer.php'; ?>
<?php require_once 'view/footer.php'; ?>
