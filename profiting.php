<?php
	$page = 'profiting';
	$menu = 'products';
	require_once 'view/header.php';
?>

<section>
	<img src="/model/images/products_profiting-main.jpg" alt="products_profiting-main">
</section>

<article class="article-center">
	<h1>Faster healing.<br>higher profits.</h1>
	<p>
		MasterPuls Shockwave treatment provides an ideal way for healthcare practitioners to
		expand their business. The MasterPuls line of shockwave machines are easy to use, so
		staff members can be trained to administer therapy. It also treats a wide array of
		conditions covered by third-party insurance including, Plantar Fasciitis, Non-Healing
		Ulcers, Tendinopathies, Bursitis, Trigger Point Therapy and many more. But beyond this,
		the earning potential is staggering.
	</p>
	<p><strong>
		Discover how you can expand your business while providing leading edge physical therapy.
	</strong></p>
</article>

<article>
	<h2>What you could be making:</h2>
	<div>
		<span>$150<sub>per session</sub></span>
		<span>4mins<sub>avg. treatment time</sub></span>
		<span>x3<sub>sessions</sub></span>
		<span>$450<sub>per patient, per month</sub></span>
	</div>

	<h2>If you treat 10 patients a month:</h2>
	<div>
		<span>$150<sub>per session</sub></span>
		<span>x3<sub>sessions per patient</sub></span>
		<span>$4,500<sub>treating 10 patients a month</sub></span>
	</div>
</article>

<article class="article-center">
	<h2>
		Treating <strong>just 1 patient a month</strong> will more than cover the monthly
		leasing cost of a masterpuls ultra device.
	</h2>
</article>

<?php include 'view/modules/models-footer.php'; ?>
<?php require_once 'view/footer.php'; ?>
