<?php
	$page = 'models-vs-competitors';
	$menu = 'products';
	require_once 'view/header.php';
?>

<article class="article-center">
	<h1>When you’re the leader, everyone else follows</h1>
	<p>Shockwave Canada machines are Health Canada and FDA approved for a reason.</p>
	<iframe width="800" height="450" src="https://www.youtube-nocookie.com/embed/hClaRbqtH6o?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>

	<table cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<td></td><td>STORZ</td><td>DJO</td><td>ZIMMER</td><td>BTL</td>
			</tr>
		</thead>
		<tbody>
			<tr><td>Controls in the handpiece</td><td class="yes"></td><td class="no"></td><td class="no"></td><td class="no"></td></tr>
			<tr><td>Spine applicator head</td><td class="yes"></td><td class="no"></td><td class="no"></td><td class="no"></td></tr>
			<tr><td>Hand made in Switzerland</td><td class="yes"></td><td class="no"></td><td class="no"></td><td class="no"></td></tr>
			<tr><td>Models (MP50, MP100, MP200, Duolith) </td><td>4</td><td>2</td><td>1</td><td>1</td></tr>
			<tr><td>Radial shockwave and focused machines </td><td class="yes"></td><td class="no"></td><td class="no"></td><td class="no"></td></tr>
			<tr><td>Double-blind randomized studies </td><td class="yes"></td><td class="no"></td><td class="no"></td><td class="no"></td></tr>
			<tr><td>Extensive research and development </td><td class="yes"></td><td class="no"></td><td class="no"></td><td class="no"></td></tr>
			<tr><td>Best value</td><td class="yes"></td><td class="no"></td><td class="no"></td><td class="no"></td></tr>
			<tr><td>24hr professional advisors (Medical, Chiro, Massage, Physio) </td><td class="yes"></td><td class="no"></td><td class="no"></td><td class="no"></td></tr>
			<tr><td>World class institutions using device</td><td class="yes"></td><td class="no"></td><td class="no"></td><td class="no"></td></tr>
			<tr><td>Vibration technology option</td><td class="yes"></td><td class="yes"></td><td class="no"></td><td class="no"></td></tr>
			<tr><td>Health Canada and FDA approved</td><td class="yes"></td><td class="no"></td><td class="no"></td><td class="no"></td></tr>
		</tbody>
	</table>
</article>

<?php include 'view/modules/compare-footer.php'; ?>
<?php require_once 'view/footer.php'; ?>
