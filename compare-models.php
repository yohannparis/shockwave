<?php
	$page = 'models';
	$menu = 'products';
	require_once 'view/header.php';
?>

<section>
	<img src="/model/images/products_comparemodels-main.jpg" alt="compare" width="100%">
</section>

<article>
	<br><br>
	<h1>Choosing shockwave therapy is a choice.<br>We have plenty to choose from.</h1>
	<br><br>

	<div>
		<h3>MP50</h3>
		<span><img src="/model/images/products_models-mp50.jpg" alt="MP50"></span>
		<p>
			<strong>Portability</strong>
			<br>Yes
		</p><p>
			<strong>Silent Compressor</strong>
			<br>Yes
		</p><p>
			<strong>Frequency Pressure</strong>
			<br>1-15hz/4.0 Bar
		</p>
	</div>

	<div>
		<h3>MP100</h3>
		<span><img src="/model/images/products_models-mp100.jpg" alt="MP100"></span>
		<p>
			<strong>Portability</strong>
			<br>Yes
		</p><p>
			<strong>Silent Compressor</strong>
			<br>Yes
		</p><p>
			<strong>Frequency Pressure</strong>
			<br>1-21hz/5.0 Bar
		</p>
	</div>

	<div>
		<h3>MP200</h3>
		<span><img src="/model/images/products_models-mp200.jpg" alt="MP200"></span>
		<p>
			<strong>Portability</strong>
			<br>Yes
		</p><p>
			<strong>Silent Compressor</strong>
			<br>Yes
		</p><p>
			<strong>Frequency Pressure</strong>
			<br>1-21hz/1.0-5.0 Bar
		</p>
	</div>

</article>


<?php include 'view/modules/surpassing-footer.php'; ?>
<?php require_once 'view/footer.php'; ?>
