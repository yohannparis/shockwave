<?php
	$page = 'models';
	$menu = 'products';
	require_once 'view/header.php';
?>

<section>
	<img src="/model/images/products_models-main.jpg" alt="MP50" width="100%">
</section>

<article class="article-center hero">
	<h1>To provide the latest in care, you need the latest in&nbsp;technology</h1>
	<p>
		In the world of shockwave therapy equipment, Storz Medical is truly unrivaled.
		It’s why Shockwave Canada has partnered exclusively with Storz to bring you
		legendary Swiss design that offers superior versatility with maximal results.
		Only Storz offers hand unit controls so healthcare practitioners can provide
		the best in “hands-on” care. And it’s all made accessible by offering the most
		affordable unit in the&nbsp;industry.
	</p>
	<p><strong>
		Discover how Shockwave Canada leverages over 75 years of medical research and
		development through world-renowned medical manufacturer,&nbsp;STORZ.
	</strong></p>
</article>

<article>

	<div>
		<h3>MASTERPULS «ULTRA» MP50</h3>
		<span><img src="/model/images/products_models-mp50.jpg" alt="MP50"></span>
		<p><strong>Compact advanced radial<br>shock wave therapy&nbsp;system</strong></p>
		<a href="/mp50">Read More</a>
	</div>

	<div>
		<h3>MASTERPULS «ULTRA» MP100</h3>
		<span><img src="/model/images/products_models-mp100.jpg" alt="MP100"></span>
		<p><strong>Advanced mobile radial<br>shock wave therapy&nbsp;system</strong></p>
		<a href="/mp100">Read More</a>
	</div>

	<div>
		<h3>MASTERPULS «ULTRA» MP200</h3>
		<span><img src="/model/images/products_models-mp200.jpg" alt="MP200"></span>
		<p><strong>Advanced high-frequency radial<br>shock wave therapy&nbsp;system</strong></p>
		<a href="/mp200">Read More</a>
	</div>

</article>

<?php include 'view/modules/surpassing-footer.php'; ?>
<?php require_once 'view/footer.php'; ?>
