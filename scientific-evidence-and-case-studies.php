<?php
	function word_wrapper($text,$minWords = 3) {
		$return = $text;
		$arr = explode(' ',$text);
		if(count($arr) >= $minWords) {
			$arr[count($arr) - 2].= '&nbsp;'.$arr[count($arr) - 1];
			array_pop($arr);
			$return = implode(' ',$arr);
		}
		return $return;
	}

	$page = 'scientific-evidence-and-case-studies';
	$menu = 'science';
	require_once 'view/header.php';
?>

<article>
	<img src="/model/images/home_main.jpg" alt="scientific-evidence-and-case-studies">

	<h1>The proof is in the science</h1>
	<p>
		If there are any more reasons to not support a non-surgical, non-invasive, affordable
		treatment to accelerate healing, let them all be laid to rest.
	</p>
	<p><a href="https://get.adobe.com/reader/">
		You may need to download Adobe Acrobat Reader in order to view these studies.
	</a></p>
</article>

<article>
	<ul>
		<?php
			// Science and case studies PDF
			$documents[] = array(
				'title' => 'Radial Extracorporeal Shock Wave Therapy Is Safe and Effective in the Treatment of Chronic Recalcitrant Plantar Fasciitis',
				'by' => 'Ludger Gerdesmeyer, MD, PhD, Carol Frey, MD, Johannes Vester, PhD, Markus Maier, PhD, Lowell Weil Jr, DPM, Lowell Weil Sr, DPM, Martin Russlies, PhD, John Stienstra, DPM, Barry Scurran, DP, Keith Fedder, MD, Peter Diehl, MD, Heinz Lohrer, MD, Mark Henne, MD, and Hans Gollwitzer, MD',
				'pdf' => 'docline_25834396.pdf');
			$documents[] = array(
				'title' => 'Clinically Relevant Effectiveness of Focused Extracorporeal Shock Wave Therapy in the Treatment of Chronic Plantar Fasciitis. A Randomized, Controlled Multicenter Study',
				'by' => 'Hans Gollwitzer, MD, Amol Saxena, DPM, Lawrence A. DiDomenico, DPM, Louis Galli, DPM, Richard T. Bouche, DPM, David S. Caminear, DPM, Brian Fullem, DPM, Johannes C. Vester,  ́Carsten Horn, MD, Ingo J. Banke, MD, Rainer Burgkart, MD, and Ludger Gerdesmeyer, MD',
				'pdf' => 'Clinically_Relevant_Focused_Shockewave_JBJS-Gollwitzer.pdf');
			$documents[] = array(
				'title' => 'Study #2 - Ultrasonographic Evaluation of Low Energy Extracorporeal Pulse- Activated Therapy (EPAT) for Chronic Plantar Fasciitis',
				'by' => 'Robert Gordon, MD; Charles Wong, BHSc; Eric J. Crawford, BHSc Toronto, Canada',
				'pdf' => 'Shockwave_Brochure_5290noPageum.pdf');
			$documents[] = array(
				'title' => 'Shockwave Therapy for the Treatment of Chronic Proximal Hamstring Tendinopathy in Professional Athletes',
				'by' => 'Angelo Cacchio, Jan D. Rompe, John P. Furia, Piero Susi, Valter Santilli and Fosco De Paulis',
				'pdf' => 'AJSM_2011_39.pdf');
			$documents[] = array(
				'title' => 'Extracorporeal Shock-Wave Therapy Compared with Surgery for Hypertrophic Long-Bone Nonunions',
				'by' => 'Angelo Cacchio, Lucio Giordano, Olivo Colafarina, Jan D. Rompe, Emanuela Tavernese, Francesco Ioppolo, Stefano Flamini, Giorgio Spacca and Valter Santilli',
				'pdf' => 'ESWTtibianonunionJBJS.pdf');
			$documents[] = array(
				'title' => 'Abstract-Heel Pain Treatment Results using Extracorporeal Pulse Activation Therapy (EPAT) vs. Extracorporeal Shock Wave Therapy (ESWT).',
				'by' => 'Angela Drury-Schimberg, D.P.M., F.A.C., F.A.S.',
				'pdf' => 'Abstract_-_Schimberg_2006-09.pdf');
			$documents[] = array(
				'title' => 'The American Journal of Sports Medicine - Eccentric Loading, Shock-Wave Treatment, or a Wait-and-See Policy for Tendinophathy of the Main Body of Tendo Achillis',
				'by' => 'Jan D. Rompe, M.D., Bernhard Nafe, M.D., John P. Furia, M.D. Ph.D., and Nicola Maffulli, M.D., Ph.D., F.R.C.S.(Orth)',
				'pdf' => 'AJSM374-Gordon.pdf');
			$documents[] = array(
				'title' => 'The American Journal of Sports Medicine - High-Energy Extracorporeal Shock Wave Therapy as a Treatment for Insertional Achilles Tendinopathy',
				'by' => 'John Patrick Furia, M.D.',
				'pdf' => 'AJSM733-Gordon.pdf');
			$documents[] = array(
				'title' => 'The American Journal of Sports Medicine - Extracorporeal Shockwave for Chronic Patellar Tendinopathy',
				'by' => 'Ching-Jen Wang M.D., Jih-Yang Ko M.D., Yi-Sheng Chan M.D., Lin-Hsiu Weng M.D. and Shan-Lin Hsu M.D.',
				'pdf' => 'AJSM972-Gordon.pdf');
			$documents[] = array(
				'title' => 'Low-Energy Extracorporeal Shock Wave Therapy as a Treatment for Medial Tibial Stress Syndrome',
				'by' => 'Jan D. Rompe, Angelo Cacchio, John P. Furia and Nicola Maffulli',
				'pdf' => 'AJSM_2010_38.pdf');
			$documents[] = array(
				'title' => 'Physical Therapy - Effectiveness of Radial Shock-Wave Therapy for Calcific Tendinitis of the Shoulder: Single-Blind, Randomized Clinical Study',
				'by' => 'Angelo Cacchio, Marco Paoloni, Antonio Barile, Romildo Don, Fosco de Paulis, Vittorio Calvisi, Alberto Ranavolo, Massimo Frascarelli, Valter Santilli, Giorgio Spacca',
				'pdf' => 'calcifying_tendontis_pagani.pdf');
			$documents[] = array(
				'title' => 'Radial Shockwave Therapy in Calcific Tendinitis of the Rotator Cuff',
				'by' => 'Dr. P. Magosch, ATOS Clinic Heidelberg (Germany)',
				'pdf' => 'radial_swt_calcifrotator.pdf');
			$documents[] = array(
				'title' => 'Evaluation of Low-Energy Extracorporeal Shock-Wave Application for Treatment of Chronic Plantar Fasciitis',
				'by' => 'Jan D. Rompe, MD, Carsten Schoellner, MD, And Bernhard Nafe, MD',
				'pdf' => 'TreatmentofChronic.pdf');
			$documents[] = array(
				'title' => 'The American Journal of Orthopedics - Safety and Efficacy of ESWT for Chronic Lateral Epicondylitis',
				'by' => 'John P. Furia, M.D.',
				'pdf' => 'EfficacyofESWTfor.pdf');
			$documents[] = array(
				'title' => 'Foot and Ankle International - ESWT for the Treatment of Plantar Fasciitis',
				'by' => 'George Theodore, M.D., Matthias Buch, M.D., Annunziato Amendola, M.D., F.R.C.D., Christine Bachmann, M.D., Lamar L. Fleming, M.D., Chistopher Zingas, M.D.',
				'pdf' => 'ESWTfortheTreatment.pdf');
			$documents[] = array(
				'title' => 'The American Journal of Sports Medicine - Shock Wave Therapy for Patients with Lateral Epiconylitis of the Elbow',
				'by' => 'Ching-Jen Wang, M.S. & Han-Shiang Chen, M.D.',
				'pdf' => 'LateralEpicondylitis.pdf');
			$documents[] = array(
				'title' => 'Radial Extracorporeal Shock Wave Therapy (rESWT) in Chronic Plantar Heel Pain - a RCT',
				'by' => 'L. Gerdesmeyer, L. Weil, B. Scrurran, J. Stienstra, C. Frey, K. Fedder, M. Maier, M. Henne, M. Russlies, H. Lohrer, J. Vester',
				'pdf' => 'Abs_RESW_ICP_heel.pdf');
			$documents[] = array(
				'title' => 'Der niedergelassene Chirurg - Radial Shockwave Therapy in Heel Spur (Plantar Fasciitis)',
				'by' => 'G. Haupt, R. Diesch, T. Straub, E. Penninger, T. Frölich, J. Schöll, H, Lohrer, T. Senge',
				'pdf' => 'qradial_swave_THP.pdf');
			$documents[] = array(
				'title' => 'Techniques in Foot and Ankle Surgery - Shock Wave Therapy for Treatment of Foot and Ankle Conditions',
				'by' => 'Alastair Younger, MB, ChB, FRCSC',
				'pdf' => 'Shock_Wave_foot.pdf');

			foreach ($documents as $document) {
		?>
			<li>
				<p><?=word_wrapper($document['title']);?></p>
				<p>By: <?=word_wrapper($document['by']);?></p>
				<a target="_blank" href="/model/pdf/<?=$document['pdf'];?>">Download</a>
			</li>
		<?php
			}

			// links to website
			$links[] = array(
				'title' => 'Extracorporeal shock wave therapy for the treatment of chronic calcifying tendonitis of the rotator cuff: a randomized controlled trial*',
				'by' => 'Gerdesmeyer L, Wagenpfeil S, Haake M, Maier M, Loew M, Wortler K, Lampe R, Seil R, Handle G, Gassel S, Rompe JD.',
				'link' => 'http://jama.jamanetwork.com/article.aspx?articleid=197669');
			$links[] = array(
				'title' => 'Antibacterial effects of extracorporeal shock waves*',
				'by' => 'Gerdesmeyer L, von Eiff C, Horn C, Henne M, Roessner M, Diehl P, Gollwitzer H. Klinik und Poliklinik fur Orthopadie und Sportorthopadie der Technischen Universitat Munchen, Munchen, Germany',
				'link' => 'http://www.umbjournal.org/article/S0301-5629(04)00233-9/abstract');
			$links[] = array(
				'title' => 'Repetitive low-energy shock wave application without local anesthesia is more efficient than repetitive low-energy shock wave application with local anesthesia in the treatment of chronic plantar fasciitis*',
				'by' => 'Rompe JD, Meurer A, Nafe B, Hofmann A, Gerdesmeyer L.',
				'link' => 'http://www.sciencedirect.com/science/article/pii/S0736026604002323');
			/*$links[] = array(
				'title' => 'Extracorporeal Shock Wave Therapy without Local Anesthesia for Chronic Lateral Epicondylitis*',
				'by' => 'Frank A. Pettrone, MD1 and Brian R. McCall, MD2 Investigation performed at the Virginia Hospital Center, Arlington, Virginia, and the Department of Orthopaedic Surgery, Georgetown University Hospital, Washington, DC',
				'link' => 'xxx');*/
			foreach ($links as $link) {
		?>
			<li>
				<p><?=word_wrapper($link['title']);?></p>
				<p>By: <?=word_wrapper($link['by']);?></p>
				<a class="link" href="<?=$link['link'];?>">View Online</a>
				<span>*Fee to see whole paper</span>
			</li>
		<?php } ?>
	</ul>
</article>

<?php include 'view/modules/models-footer.php'; ?>
<?php require_once 'view/footer.php'; ?>
