<?php
	$page = 'testimonials';
	$menu = 'science';
	require_once 'view/header.php';
?>

<article class="article-center">
	<h1>Don’t take our word for it, take theirs</h1>
	<p>
		See what leading medical practitioners have to say about the Storz MASTERPULS
		devices and the effectiveness of shockwave therapy.
	</p>
</article>

<article>
	<ul class="page-testimonials">
		<?php
			// Check if a testimonials has been selected in the url
			if (!isset($_GET['t'])){ $_GET['t'] = '01'; }
			$start = $_GET['t'];

			// The testimonials
			$testimonials['01'] = array('title' => 'Dr. Davis E. Lindsay', 'src' => 'dr-david-e-lindsay.png');
			$testimonials['02'] = array('title' => 'Moore Chiropractic & Massage Therapy Clinic', 'src' => 'moore-chiropractic-and-massage-therapy-clinic.jpg');
			$testimonials['03'] = array('title' => 'Dr. Randy Kobayashi', 'src' => 'dr-Randy-Kobayashi.jpg');
			$testimonials['04'] = array('title' => 'Queen\'s University', 'src' => 'queen-s-university.jpg');
			$testimonials['05'] = array('title' => 'Canadian Memorial Chiropractic College', 'src' => 'canadian-memorial-chiropractic-college.jpg');
			$testimonials['06'] = array('title' => 'Medical Surgical Foot Center', 'src' => 'medical-surgical-foot-cente.jpg');
			$testimonials['07'] = array('title' => 'R.L. Goossens, D.P.M', 'src' => 'rl-goossens.jpg');
			$testimonials['08'] = array('title' => 'Dr. Robert Gordon, M.D., FRCS (C)', 'src' => 'dr-robert-gordon.jpg');
			$testimonials['09'] = array('title' => 'Bayview Physiotherapy', 'src' => 'bayview_physiotherapy.jpg');
			$testimonials['10'] = array('title' => 'Toronto Athletic Club', 'src' => 'toronto-athletic-club.jpg');
			$testimonials['11'] = array('title' => 'Dr. Glenn Stirling, D.C.', 'src' => 'dr-glenn-stirling.jpg');
			$testimonials['12'] = array('title' => 'Synergy Health Management Ltd.', 'src' => 'synergy-health-management.jpg');

			foreach ($testimonials as $key => $testimonial) {
		?>
			<li data-src="<?=$testimonial['src'];?>" <?php if($key == $start){ echo 'class="active"'; } ?>><?=$testimonial['title'];?></li>
		<?php } ?>
	</ul>

	<div>
		<img id="page-testimonials-image" src="/model/testimonials/<?=$testimonials[$start]['src'];?>" alt="<?=$testimonials[$start]['title'];?>">
		<a href="javascript:void(0)" class="page-testimonials-arrow prev">&lt; Previous</a>
		<a href="javascript:void(0)" class="page-testimonials-arrow next">Next &gt;</a>
	</div>
</article>

<script type="text/javascript">
  function move(direction) {
    var list = document.querySelectorAll('.page-testimonials li'); // Get the list of artworks
    var current = document.querySelector('.page-testimonials .active'); // Get the current one
    current.classList.remove('active'); // Remove is marker

    switch (direction){ // Select what direction we are going

      case 'next': // If we are moving forward
        var index = Array.prototype.slice.call(list).indexOf(current); // Position of the current one
        if (index >= list.length-1){ // But we are at the end of the list
          var next = list[0]; // go back to the beginning
        } else {
          var next = list[index+1]; // keep moving forward
        }
        break;

      case 'prev': // If we are going backward
        var index = Array.prototype.slice.call(list).indexOf(current); // Position of the current one
        if (index < 1){ // if we are at the beggining
          var next = list[list.length-1]; // go to the end
        } else {
          var next = list[index-1]; // keep moving backward
        }
        break;

      default: // Or it's a specific direction
        var index = Array.prototype.slice.call(list).indexOf(direction); // Find the position of the specific one
        var next = list[index]; // display the selected one
        break;
    }

    next.classList.add('active'); // Add the marker
    document.querySelector('#page-testimonials-image').src = '/model/testimonials/' + next.getAttribute('data-src'); // Show the image
  }

  window.onload = function() {
    var images = document.querySelectorAll('.page-testimonials li');
    //images[0].classList.add('active'); // Display the first image

    // Go directly to the appropriate image
    for (var i = images.length - 1; i >= 0; i--) {
      images[i].addEventListener('click', function(){
        move(this);
      });
    };

    // On the click of one of the arrow
    document.querySelector('.page-testimonials-arrow.prev').addEventListener('click', function() { move('prev'); });
    document.querySelector('.page-testimonials-arrow.next').addEventListener('click', function() { move('next'); });

    // On the press of one of the keyboard arrow
    document.onkeydown = function(e) {
      e = e || window.event;
      switch (e.keyCode) {
        case 37: // Left key
          move('prev');
          break;
        case 39: // Right key
          move('next');
          break;
      }
    };
  };
</script>

<?php include 'view/modules/models-footer.php'; ?>
<?php require_once 'view/footer.php'; ?>
