<?php
	$page = 'training-and-education';
	$menu = 'services';
	require_once 'view/header.php';

	$show_training = false;
	$show_education = false;

	if (isset($_POST['password'])){

		if (isset($_POST['submit-training']) and $_POST['password'] === 'Shockwave_9550'){
			$show_training = true;
		}

		if (isset($_POST['submit-education']) and $_POST['password'] === 'Shockwave_9550'){
			$show_education = true;
		}

		/*
		if (isset($_POST['submit-education']) and $_POST['password'] !== ''){
			// Connect to the database
			$link = mysql_connect('shockwavecanadainc.netfirmsmysql.com', 'shockwave', 'shockwave');
			mysql_select_db('shockwave', $link);

			// Check if the password exist
			$query = "SELECT * FROM `seminar` WHERE `password` = '".$_POST['password']."'";
			$result = mysql_query($query);

			// If the password does exist
			if (mysql_num_rows($result) > 0){

				// Get the password and date of last use
				$result = mysql_fetch_assoc($result);

				// If the First used is empty we fill it in
				if(is_null($result['used'])){
					$query = "UPDATE seminar SET used = '".date('Y-m-d')."' WHERE `password` = '".$result['password']."'";
					$resultUpdate = mysql_query($query);
					$result['used'] = date('Y-m-d');
				}

				// Check the deadline
				$endofuse = strtotime('+3 days', strtotime($result['used']));

				// Check if it's been 2 days since first used
				// Then we send them back
				if ($endofuse < time()){
					header('Location: /training-video');

				// We show the video
				} else {
					$show_education = true;
				}
			}
		}
		*/
	}
?>

<article class="article-center">
	<img src="/model/images/services_training-main.jpg" alt="MP50" width="100%">
	<h1>Using shockwave machines.<br>Shockingly simple.</h1>

	<div>
		<h2>Training video</h2>
		<p>To watch the training video please enter your password.</p>
		<form action="training-and-education" method="post">
			<input type="text" name="password"><input type="submit" name="submit-training" value="SUBMIT">
		</form>
	</div>

	<div>
		<h2>Educational seminar</h2>
		<p>
			To watch a video of the educational seminar please enter your password.
			<?php /*Please note that passwords expire two days after first&nbsp;use.*/ ?>
		</p>
		<form action="training-and-education" method="post">
			<input type="text" name="password"><input type="submit" name="submit-education" value="SUBMIT">
		</form>
	</div>

</article>

<?php if($show_training){ ?>
	<article class="article-center">

		<h2>Indications and uses</h2>
		<video src="/model/training/Indications_and_uses.mp4" width="80%" alt="Training-video" controls></video>
		<br>

		<h2>Revision of the Ultra handpiece</h2>
		<video src="/model/training/Storz-Medical_Video04_FINAL_EN.mp4" width="80%" alt="Training-video" controls></video>
		<br>

		<h2>Revision of the Elite handpiece</h2>
		<video src="/model/training/revision_18080_01_lq.mp4" width="80%" alt="Training-video" controls></video>
	</article>
<?php } ?>

<?php if($show_education){ ?>
	<article class="article-center">
		<h2>Ludger Gerdesmeyer - ESWT Technique</h2>
		<video src="/model/educational/LG_Part1.mp4" width="648" height="364" controls></video><br><br>
		<h2>Ludger Gerdesmeyer - Principles of Shockwave Application</h2>
		<video src="/model/educational/LG_Part2.mp4" width="648" height="364" preload="none" controls></video><br><br>
		<h2>Ludger Gerdesmeyer - ESWT in Sports medicine</h2>
		<video src="/model/educational/LG_Part3.mp4" width="648" height="364" preload="none" controls></video><br><br>
		<h2>Ludger Gerdesmeyer - Shockwave Therapy indications</h2>
		<video src="/model/educational/LG_Part4.mp4" width="648" height="364" preload="none" controls></video><br><br>
		<h2>Chris Broadhurst - ESWT</h2>
		<video src="/model/educational/CB_Part1.mp4" width="648" height="364" preload="none" controls></video><br><br>
		<h2>Chris Broadhurst - Patient Demonstration</h2>
		<video src="/model/educational/CB_Part2.mp4" width="648" height="364" preload="none" controls></video><br><br>
	</article>
<?php } ?>

<?php include 'view/modules/models-footer.php'; ?>
<?php require_once 'view/footer.php'; ?>
