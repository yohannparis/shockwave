<?php
	$page = 'contact-us';
	require_once 'view/header.php';
?>

<article>
	<h1>CONTACT US</h1>
	<p>
		<strong>Shockwave Canada</strong>
		<br>89 Humber College Blvd., Suite 106
		<br>Toronto, ON M9V 1B8 Canada
		<br>
		<br>Phone: (416) 741-SHOC(7462)
		<br>Toll Free: 1(888) 741-SHOC(7462)
		<br>Fax: (416) 741-8424
		<br>
		<br>Sales: <a href="mailto:brian@shockwavecanadainc.ca">brian@shockwavecanadainc.ca</a>
		<br>Talk to the Doctor: <a href="mailto:rob@shockwavecanadainc.ca">rob@shockwavecanadainc.ca</a>
		<br>Info: <a href="mailto:brian@shockwavecanadainc.ca">brian@shockwavecanadainc.ca</a>
		<br>Service: <a href="mailto:john@shockwavecanadainc.ca">john@shockwavecanadainc.ca</a>
	</p>

	<p>
		<strong>For Treatment Information:</strong>
		<br>Dr. Rob Gordon
		<br>Phone: (416) 545-1166
		<br><a href="http://shockwavedoc.com">shockwavedoc.com</a>
	</p>
</article>

<?php include 'view/modules/models-footer.php'; ?>
<?php require_once 'view/footer.php'; ?>
