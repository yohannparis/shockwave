<?php
	$page = 'pricing-and-purchasing';
	$menu = 'products';
	require_once 'view/header.php';
?>

<section>
	<img src="/model/images/products_purchasing-main.jpg" alt="products_purchasing-main">
</section>

<article class="article-center">
	<h1>Nothing should get in the way of patient care</h1>
	<p>
		A healthcare professional’s goal is to help their patients. It’s why they’ve sacrificed
		so much for the privilege. At Shockwave Canada, we believe that nothing should stand in the
		way of patient care. It’s why we offer the most affordable shockwave therapy unit in the
		business. What’s more, we give our customers various ways to purchase or lease so they can
		start administering care immediately.
	</p>
	<p><strong>
		Discover how you can start treating patients today, and start growing your practice tomorrow.
	</strong></p>
</article>

<article>
	<ul>
		<li>
			<span><strong>MP50</strong> Tissue Regenerator Shockwave Machine</span>
			<span>Purchasing Options: <strong>Buy or Lease</strong></span>
		</li>
		<li>
			<span><strong>MP50</strong> With V-Actor Handpiece</span>
			<span>Purchasing Options: <strong>Buy or Lease</strong></span>
		</li>
		<li>
			<span><strong>MP100</strong> Tissue Regenerator Shockwave Machine</span>
			<span>Purchasing Options: <strong>Buy or Lease</strong></span>
		</li>
		<li>
			<span><strong>MP100</strong> With V-Actor Handpiece</span>
			<span>Purchasing Options: <strong>Buy or Lease</strong></span>
		</li>
	</ul>
</article>

<article class="article-center">
	<a href="/contact-us"><h2>
		Click here to Contact shockwave canada for pricing
	</h2></a>
</article>

<?php include 'view/modules/models-footer.php'; ?>
<?php require_once 'view/footer.php'; ?>
