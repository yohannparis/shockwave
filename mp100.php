<?php
	$page = 'mp';
	$menu = 'products';
	require_once 'view/header.php';
?>

<article>
	<img src="/model/images/products_mp100-main.jpg" alt="MP100">
	<h3>Models</h3>
	<h1>MASTERPULS «ULTRA» MP100</h1>
	<p>
		All the versatility of the 50, the MP 100 features an integrated controls hand-piece,
		as well as a built-in, high-performance compressor for more power and effective results,
		with the added convenience of multiple hand-pieces. And all featuring deep impact shock
		transmitters.
	</p>

	<span>
		<a href="/model/products/mp100brochure.pdf">Download English Brochure</a>
		<a href="/model/products/mp100brochurefr.pdf">Download French Brochure</a>
		<a href="/model/products/mp100flyer.pdf">Download English Flyer</a>
		<a href="/model/products/mp100booklet.pdf">Download English Booklet</a>
	</span>
</article>

<article>

	<div>
		<h3>Key Features</h3>
		<ul>
			<li>Newly developed hand control, with all main operating elements integrated into the handpiece. Frequency, energy levels and number of shocks applied can be adjusted directly via selector buttons. Only product of its kind on the market. This allows for safer treatments on patients as changes can be made to settings without looking away from the patient</li>
			<li>Compact design</li>
			<li>Built-in high-performance compressor makes the system even more powerful and provides excellent therapy success rates</li>
			<li>Low maintenance costs</li>
			<li>Combinable handpieces</li>
			<li>Radial Shockwave therapy with various shock transmitters</li>
			<li>Vibration therapy (V-ACTOR®)</li>
		</ul>
		<a href="/accessories-and-add-ons">View optional accessories</a>
	</div>

	<div>
		<h3>Specifications</h3>
		<p>
			<strong>Pulse Frequency/Pressure</strong>
			<br>• Radial shockwave therapy: 1 – 21 Hz/1 – 5 bar
			<br>• Vibration therapy (V-ACTOR®): 1 – 35 Hz/1 – 5 bar
		</p><p>
			<strong>Oscillating ‘D-ACTOR®’ Technology</strong>
			<br>• For better myofascial Trigger Point therapy
		</p><p>
			<strong>'Deep Impact' Shock Transmitter</strong>
			<br>• For the treatment of deep pain regions
		</p><p>
			<strong>'CERAma-xTM' Shock Transmitter</strong>
			<br>• Elastic shock transmitter for Shockwave
		</p><p>
			<strong>‘V-ACTOR®’ handpiece</strong>
			<br>• For muscle and connective tissue activation/smoothing
		</p>

	</div>

</article>

<?php include 'view/modules/compare-footer.php'; ?>
<?php require_once 'view/footer.php'; ?>
