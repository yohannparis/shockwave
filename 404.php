<?php
	$page = '404';
	require_once 'view/header.php';
?>

<article class="article-center">
	<h1>Error 404</h1>
	<h2>Sorry, we couldn't find the page you are looking for.</h2>
</article>

<?php require_once 'view/footer.php'; ?>
