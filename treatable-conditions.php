<?php
	$page = 'treatable-conditions';
	$menu = 'science';
	require_once 'view/header.php';
?>

<article class="article-center">
	<h1>The world’s most effective shockwave treatment</h1>
	<p>
		It’s no surprise that the benefits of shockwave therapy as a
		non-surgical way of treating a wide array of soft tissue conditions,
		is clearly becoming the most popular treatment.
	</p>
</article>

<article>
	<ul>
		<li><div>
			<span>Examples of indications for the&nbsp;Storz masterpuls&reg;&nbsp;ultra</span>
		</div></li>
		<li>
			<div><span>Patellar tendinitis</span></div>
			<img src="/model/images/science_conditions-Patellartendinitis.jpg" alt="science_conditions-Patellartendinitis">
		</li>
		<li>
			<div><span>Trigger points:<br>periarticular shoulder pain</span></div>
			<img src="/model/images/science_conditions-periarticularshoulderpain.jpg" alt="science_conditions-periarticularshoulderpain">
		</li>
		<li>
			<div><span>Metatarsalgia</span></div>
			<img src="/model/images/science_conditions-Metatarsalgia.jpg" alt="science_conditions-Metatarsalgia">
		</li>
		<li>
			<div><span>TRIGGER POINTS:<br>CALF MUSCLE SHORTENING</span></div>
			<img src="/model/images/science_conditions-calfmuscleshortening.jpg" alt="science_conditions-calfmuscleshortening">
		</li>
		<li>
			<div><span>TIBIAL STRESS SYNDROME</span></div>
			<img src="/model/images/science_conditions-Tibialstress.jpg" alt="science_conditions-Tibialstress">
		</li>
		<li>
			<div><span>Achilles Tendinopathy</span></div>
			<img src="/model/images/science_conditions-Achillodynia.jpg" alt="science_conditions-Achillodynia">
		</li>
		<li>
			<div><span>PLANTAR FASCIITIS</span></div>
			<img src="/model/images/science_conditions-plantarfaciitis.jpg" alt="science_conditions-plantarfaciitis">
		</li>
		<li>
			<div><span>CALCIFIC TENDINITIS</span></div>
			<img src="/model/images/science_conditions-calcifictendinitis.jpg" alt="science_conditions-calcifictendinitis">
		</li>
	</ul>
</article>

<article>
	<h2>Indications:</h2>
	<ul>
		<li>TREATMENT OF TENDINOPATHIES</li>
		<li>HAMSTRINGS</li>
		<li>MYOFASCIAL TRIGGER POINTS</li>
		<li>ACHILLES TENDINOPATHY</li>
		<li>BURSITIS</li>
		<li>HALLUX RIGIDUS</li>
		<li>NON-HEALING ULCERS</li>
		<li>TENDONITIS</li>
		<li>SCAR TISSUE</li>
		<li>JUMPERS KNEE</li>
		<li>CALCIFIC ROTATOR CUFF TENDINITIS</li>
		<li>TRIGGER POINT THERAPY</li>
		<li>NON UNIONS</li>
		<li>HALLUX RIGIDUS</li>
		<li>SHOULDER PAIN</li>
		<li>TENNIS ELBOW</li>
		<li>PATELLAR TENDONITIS</li>
		<li>PLANTAR FASCIITIS/HEEL SPUR</li>
		<li>SHIN SPLINTS</li>
		<li>STRESS FRACTURES</li>
		<li>ENHANCEMENT OF BONE HEALING</li>
		<li>MUSCLE AND CONNECTIVE TISSUE<br>ACTIVATION WITH V-ACTOR&reg;</li>
	</ul>
</article>

<article class="article-center">
	<h2>Effectiveness</h2>
	<ul>
		<li><span>91%</span> Improvement for calcific tendinitis</li>
		<li><span>83%</span> Improvement for calcific tendinitis of the shoulder</li>
		<li><span>77%</span> Improvement for tennis elbow</li>
		<li><span>76%</span> Improvement for achilles tendinopathy</li>
		<li><span>90%</span> Improvement for plantar fasciitis</li>
		<li><span>80%</span> Improvement for radial and ulnar humeral epicondylitis</li>
		<li><span>88%</span> Improvement for patellar tendinitis/ achillodynia</li>
		<li><span>80%</span> Improvement for myofascial trigger points</li>
		<li><span>90%</span> Improvement for acupuncture shockwave therapy</li>
	</ul>
</article>

<article>
	<div>
		<span>X-RAY RESULTS:<br>PRE AND POST-CALCIFIC TENDONITIS TREATMENT</span>
		<span>
			<img src="/model/images/science_effectiveness_xray.jpg" alt="science_effectiveness_xray">
			<p>PRE AND POST-SHOCKWAVE TREATMENT RADIOGRAPHS OF CALCIFIC TENDONITIS</p>
		</span>
	</div>
	<div>
		<span>EWST INDUCED NEOVASCULARIZATION AT THE TENDON-BONE JUNCTION (SHOCKWAVE VS. CONTROL)</span>
		<span>
			<img src="/model/images/science_effectiveness_graph.png" alt="science_effectiveness_graph">
			<p>Source: Dr. Wang, J ORTHOP RES. 2003;21:984989</p>
		</span>
	</div>
</article>

<?php include 'view/modules/models-footer.php'; ?>
<?php require_once 'view/footer.php'; ?>
